First of all, thanks for contributing to this repository. There are some formats on how to do this, however.

### File Formats
The Sector Work folder, is for any files that are created to work on the file. Finished files go into the Sector Files folder and are in a *.zip format.
All files will follow this convention:
**(Facility)_(Type)_v(Airac).(Version).zip**
For example: KEWR_ASDE-X_v1610.2.zip or ZNY_VRC_v1603.4.zip or N90_vSTARS_v1612.3.zip

#### ASDE-X
The ASDE-X zip files should include both the DAY and NIGHT sector files, and a color profile.

### Credits
If you are modifying a sector file, be sure to include your name at the top of the file, and the last date the file was updated.
